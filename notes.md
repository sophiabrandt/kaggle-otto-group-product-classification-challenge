# Notes for the Kaggle Otto Group Product Classification Challenge

## Problem Statement and Requirements

The project uses the Kaggle Dataset **[Otto Group Product Classification Challenge][otto]**.  
Given that data, I want to train a machine learning model that distinguishes between 9 product categories **(predictive classification problem)**.  
I want to use **Streamlit and Docker** to set up a local environment.  
There should be a **CI/CD pipeline** that ultimately deploys the Streamlit app to **Heroku**.  
For now, I won't add tests, but using the CI/CD pipeline, it will be possible to add a testing stage to the project.

The evaluation metric for the Kaggle competition is **multi-class logarithmic loss**.  
In an initial attempt to tackle the dataset (with Jupyter Lab), I was able to score a _log loss of 1.34_.

The Streamlit app should score the same or better.

_Note_:  
The winning teams used boosting algorithms (e.g., XGBoost) to reach a better score. I will explicitliy **not** use them, as I want to focus on simpler models first.  
That means that I won't be able to reach a comparative score, but that's not my goal.

### Expectations (What I'm hoping to learn)

These are the topics I would like to learn/achieve/improve:

- how I can use Streamlit
- deployment of a machine learning app with [Docker (running as non-root user, etc.)](https://pythonspeed.com/docker/)
- working with a multi-class dataset (61,878 entries) that has 93 obfuscated features: feature engineering, feature selection, working with large data

## Resources

- [Streamlit Docs](https://docs.streamlit.io/)
- [How to Build a Streamlit App in Python](https://pythonforundergradengineers.com/streamlit-app-with-bokeh.html)
- [Deploying your Streamlit dashboard with Heroku](https://gilberttanner.com/blog/deploying-your-streamlit-dashboard-with-heroku)
- [Quickly Build and Deploy a Dashboard with Streamlit](https://towardsdatascience.com/quickly-build-and-deploy-an-application-with-streamlit-988ca08c7e83)
- [Deploy a Streamlit WebApp with Docker](https://maelfabien.github.io/project/Streamlit/)
- [Streamlit Python Tutorial (Crash Course)](https://invidio.us/watch?v=_9WiB2PDO7k)
- [How To Deploy Streamlit Apps (Using Heroku)](https://invidio.us/watch?v=skpiLtEN3yk)
- [How to Deploy Machine Learning Models](https://christophergs.com/machine%20learning/2019/03/17/how-to-deploy-machine-learning-models/)
- [Applied Machine Learning Process][process]

## Working Log

### Day 0

- setup repository (GitHub & GitLab)
- research Streamlit
- research [machine learning process][process]
- clear up problem statement for project

It takes quite some time to set up the project's README, even though I use a template. Can I make this faster?

**Two-fold problem**: I see two "buckets" in the project:

- tooling & deployment (Streamlit, Docker, Heroku, CI/CD)
- machine learning (scikit-learn, NumPy, pandas): large dataset, high number of indistinguishable features, multi-class classification

### Day 1

- setup Docker
- install Streamlit, create sample Streamlit app and run on `http://localhost:8501`
- setup GitLab CI

I originally tried to run Docker with conda, then use pip to install Streamlit.

Error: `subprocess.run(["sudo", "dbus-uuidgen", "--ensure"])` with `File or Directory not found: 'sudo'`

My guess is that that's an issue with running conda inside the Docker container.  
I use multi-stage builds. In the build stage, I install all packages. I copy these packages into the runtime image.  
I tell Docker to run the commands [inside the conda environment](https://pythonspeed.com/articles/activate-conda-dockerfile/#working).  
But there seem to be permission issues.

After some research, I decided to use a simpler approach with Python's inbuilt [virtualenv](https://pythonspeed.com/articles/activate-virtualenv-dockerfile/).  
I manage the Python dependencies with [pip-tools](https://pythonspeed.com/articles/pipenv-docker/).

I also had some hiccups with [environment variables in Docker and docker-compose](https://vsupalov.com/docker-arg-env-variable-guide/).  
I now have build args inside the Dockerfile which you can overwrite with the `docker-compose.yml` file.

### Day 2

- improve Docker and docker-compose config
- add second stage to GitLab CI with flake8 and black (Python linting tools)
- [wrote blog about this setup](https://dev.to/sophiabrandt/run-streamlit-with-docker-and-docker-compose-15d)
- start Streamlit app (problem statement)

Writing Markdown in Streamlit:

```python
import streamlit as st

st.title("Otto Group Product Classification Challenge 🛍")

st.markdown("## 1. Problem Statement")
st.markdown(
    "Given a [dataset](https://www.kaggle.com/c/otto-group-product-classification-challenge/overview) with **93 features**, create a **predictive model** which is able to **distinguish between the main product categories**."
)

st.markdown("### 1.2 Evaluation")
st.markdown(
    "The evaluation for the competition is **multi-class logarithm loss**. See [Kaggle: Evaluation.](https://www.kaggle.com/c/otto-group-product-classification-challenge/overview/evaluation)"
)
```

### Day 3

- load data with Streamlit
- add [Bokeh][bokeh]
- start exploring the data

I loaded the data with pandas and [Streamlit's caching decorator](https://docs.streamlit.io/caching.html).

Working with pandas is frictionless.  
But visualizing plots with Streamlit's matplotlib API proved to be useless. Streamlit offers the limited [`matplotlib.pyplot` API](https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.pyplot.html#module-matplotlib.pyplot) which is extremely limited.

I couldn't get the object-oriented API working.

Streamlit comes with alternative integrations for visualizing charts. I tried [Bokeh][bokeh]. The latest version of Bokeh (2.0.0) doesn't work with Streamlit, so I had to downgrade to 1.4.0.

Originally I didn't plan on learning a new library. Bokeh feels similar to Matplotlib, but it's not the same. So I now have to convert all my matplotlib snippets to Bokeh.

Example for a bar plot:

```python
import streamlit as st

from bokeh.plotting import figure
from bokeh.models import ColumnDataSource

def plot_bar_chart(series, title):
    """
    Plots a bokeh bar chart from a Pandas Series with shape (x,)
    series: pandas.Series
    title: plot title
    """
    index = series.index.tolist()
    # Convert index to strings
    index = [str(key) for key in index]
    counts = series.tolist()
    # Create data source
    source = ColumnDataSource(data=dict(index=index, counts=counts))

    # Plot figure
    p = figure(
        x_range=index, plot_height=250, title=title, toolbar_location=None, tools="",
    )
    p.vbar(x="index", top="counts", width=0.9, source=source)
    p.y_range.start = 0

    # Show plot
    st.bokeh_chart(p)
```

### Day 4

- split data into training and test set
- re-factored code to use streamlit's [magic utilities](https://docs.streamlit.io/api.html#magic-commands)

Example:

```python

# previous code
# imports, etc.

"""
## 3. Prepare Data

Split the data into **training and test set**.
"""

np.random.seed(42)

X = df.drop(["target"], axis=1)
y = df.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

"Data shapes:"
"X_train:", X_train.shape
"X_test:", X_test.shape
"y_train:", y_train.shape
"y_test:", y_test.shape

```

Output:

![Streamlit Magic Example](images/magic-example.png)

[otto]: https://www.kaggle.com/c/otto-group-product-classification-challenge/overview
[process]: https://machinelearningmastery.com/start-here/#process
[bokeh]: https://bokeh.org/
