<!-- PROJECT SHIELDS -->

[![Contributors][contributors-shield]][contributors-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![pipeline status][pipeline-shield]][pipeline-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge">
    <img src="images/logo.png" alt="Logo">
  </a>

  <h3 align="center">Otto Group Product Classification Challenge</h3>

  <p align="center">
    Exploring a Kaggle Dataset
    <br />
    <a href="https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge">View Demo</a>
    ·
    <a href="https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge/issues">Report Bug</a>
    ·
    <a href="https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [About the Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
- [Roadmap](#roadmap)
- [Contributing](#contributing)
- [License](#license)
- [Contact](#contact)
- [Acknowledgements](#acknowledgements)

<!-- ABOUT THE PROJECT -->

## About The Project

The project uses the Kaggle Dataset **[Otto Group Product Classification Challenge][otto]**.

The goal is to employ Streamlit to explore the data and to deploy a model to Heroku using Docker.

See the [Notes][notes] for more information.

[![Product Name Screen Shot][product-screenshot]][otto]

### Built With

- NumPy, Pandas, scikit-learn
- [Streamlit][streamlit]
- Docker

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

- Docker & docker-compose

### Installation

1. Clone the repository

```sh
git clone https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge.git
```

2. Run Docker

```sh
docker-compose up
```

<!-- USAGE EXAMPLES -->

## Usage

coming soon...

<!-- ROADMAP -->

## Roadmap

See the [open issues](https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge/issues) for a list of proposed features (and known issues).

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->

## License

Distributed under the MIT License. See [`LICENSE`](LICENSE) for more information.

<!-- CONTACT -->

## Contact

Sophia Brandt - [@hisophiabrandt](https://twitter.com/hisophiabrandt)

Project Link: [https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge](https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge)

<!-- ACKNOWLEDGEMENTS -->

## Acknowledgements

- [Best-README-Template](https://github.com/othneildrew/Best-README-Template)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/github/contributors/sophiabrandt/kaggle-otto-group-product-classification-challenge.svg?style=flat-square
[contributors-url]: https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge/graphs/contributors
[issues-shield]: https://img.shields.io/github/issues/sophiabrandt/kaggle-otto-group-product-classification-challenge.svg?style=flat-square
[issues-url]: https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge/issues
[license-shield]: https://img.shields.io/github/license/sophiabrandt/kaggle-otto-group-product-classification-challenge.svg?style=flat-square
[license-url]: https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge/blob/master/LICENSE.txt
[pipeline-shield]: https://gitlab.com/sophiabrandt/kaggle-otto-group-product-classification-challenge/badges/master/pipeline.svg?style=flat-square
[pipeline-url]: https://gitlab.com/sophiabrandt/kaggle-otto-group-product-classification-challenge/-/commits/master
[product-screenshot]: images/screenshot.png
[notes]: notes.md
[otto]: https://www.kaggle.com/c/otto-group-product-classification-challenge/overview
[streamlit]: https://www.streamlit.io/
