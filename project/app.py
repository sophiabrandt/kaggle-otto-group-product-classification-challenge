import numpy as np
import pandas as pd
import streamlit as st

from helpers.bokeh import plot_bar_chart
from helpers.pandas import inspect_cardinality

from sklearn.model_selection import train_test_split

st.title("Otto Group Product Classification Challenge 🛍")

"""
## 1. Problem Statement
Given a [dataset](https://www.kaggle.com/c/otto-group-product-classification-challenge/overview) with **93 features**, create a **predictive model** which is able to **distinguish between the main product categories**.

_Note_: For detailed notes, please take a look at the **[project notes](https://github.com/sophiabrandt/kaggle-otto-group-product-classification-challenge/blob/master/notes.md)**.

### 1.1 Data
> Each row corresponds to a single product. There are a total of 93 numerical features, which represent counts of different events. All features have been obfuscated and will not be defined any further.

> There are nine categories for all products. Each target category represents one of our most important product categories (like fashion, electronics, etc.). The products for the training and testing sets are selected randomly.

#### File descriptions
- trainData.csv - the training set
- testData.csv - the test set
- sampleSubmission.csv - a sample submission file in the correct format
#### Data fields
- id - an anonymous id unique to a product
- feat_1, feat_2, ..., feat_93 - the various features of a product
- target - the class of a product

### 1.2 Evaluation

The evaluation for the competition is **multi-class logarithm loss**. See [Kaggle: Evaluation.](https://www.kaggle.com/c/otto-group-product-classification-challenge/overview/evaluation)

_Note_: scikit-learn provides a utility function: [log_loss](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.log_loss.html).


## 2. Data Exploration

"""
# Load dataset, using Streamit's cache
@st.cache
def load_data():
    # load csv
    data = pd.read_csv("./data/train.csv")
    return data


df = load_data()

"Peek at the dataset:"
df.head()

# Drop id column
df = df.drop(["id"], axis=1)


if st.checkbox("Show/Hide DataFrame Information"):
    "**Dataset shape:** (after dropping the 'id' column)"
    df.shape
    "**Dataset Description:**"
    st.write(df.describe())

if st.checkbox("Show/Hide Class/Category Information"):
    "**Category Distribution:**"
    category_distribution = df.target.value_counts()
    category_distribution
    plot_bar_chart(series=category_distribution, title="Category Distribution")
    "We can see that we have to deal with class imbalances."
    "We can't collect more data."
    "We will have to address this issue later (by modifying the evaluation metrics, using class weights, upsampling or downsampling."


if st.checkbox("Show/Hide Feature Information"):
    "We'll explore the features of the dataset."
    inspect_cardinality(df)
    "---"
    '_Note_: Each feature represents a "count" of different events.'
    "Features with too many labels tend to dominate over those with only a few labels, particularly in tree-based algorithms."

"""
## 3. Prepare Data

Split the data into **training and test set**.
"""

np.random.seed(42)

X = df.drop(["target"], axis=1)
y = df.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

"Data shapes:"
"X_train:", X_train.shape
"X_test:", X_test.shape
"y_train:", y_train.shape
"y_test:", y_test.shape
