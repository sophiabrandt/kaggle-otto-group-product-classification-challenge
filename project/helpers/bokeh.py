import streamlit as st

from bokeh.plotting import figure
from bokeh.models import ColumnDataSource


def plot_bar_chart(series, title):
    """
    Plots a bokeh bar chart from a Pandas Series with shape (x,)
    series: pandas.Series
    title: plot title
    """
    index = series.index.tolist()
    # Convert index to strings
    index = [str(key) for key in index]
    counts = series.tolist()
    # Create data source
    source = ColumnDataSource(data=dict(index=index, counts=counts))

    # Plot figure
    p = figure(
        x_range=index, plot_height=250, title=title, toolbar_location=None, tools="",
    )
    p.vbar(x="index", top="counts", width=0.9, source=source)
    p.y_range.start = 0

    # Show plot
    st.bokeh_chart(p)
