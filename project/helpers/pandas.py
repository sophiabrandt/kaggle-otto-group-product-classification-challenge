import pandas as pd
import streamlit as st


def convert_into_category(df):
    """
    Converts all non-numerical columns of a pandas
    DataFrame into a categorical type.
    """
    for label, content in df.items():
        if not pd.api.types.is_numeric_dtype(content):
            df[label] = pd.Categorical(content).codes + 1
    return df


def inspect_cardinality(df):
    """
    Shows the number of labels per feature.
    """
    st.write("**Number of unique labels for each feature:**")
    for (feature, labels) in df.iteritems():
        st.write("Number of labels in {}: {}".format(feature, len(labels.unique())))
